$(function() {
    var timer;

    $('body').on('change', '.qty-field [data-item-qty]', function(){
        var form = $(this).parents('form');

        if(!$(this).data('removed')) {
            if(!($(this).val()>0)) $(this).val(1);
            if($(this).val()>99) $(this).val(99);
        } else {
            $(this).val(0);
        }

        clearTimeout(timer);
        timer = setTimeout(function(){
            if(!form.find('input[name="json"]').length) form.prepend('<input type="hidden" name="json" value="1">');
            form.ajaxSubmit(function(data){
                data = $.parseJSON(data);
                $('body').find('[data-cart-totalsumm]').text(number_format(data.total_summ, 0, '', ' ') + '.-');
                $('body').find('[data-cart-totalqty]').text(data.total_qty + ' ' + pluralForm(data.total_qty, 'товар', 'товара', 'товаров'));
            });
        }, 300);
    });

    $('body').on('click', '.qty-field .plus, .qty-field .minus', function(event) {
        event.preventDefault();
        var input = $(this).parent().find('input[type="text"]'),
            qty = parseInt(input.val()),
            max_qty = input.data('maxqty') || 99;

        qty = $(this).hasClass('plus') ? qty+1 : qty-1;

        if(qty<1) {
            qty = 1;
        } else if(qty>max_qty) {
            qty = max_qty;
        }

        input.val(qty) .change();
    });
});